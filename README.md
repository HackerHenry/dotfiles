# HackerHenry's dotfiles for the Awesome Window Manager

## I use Manjaro  as my Linux 🐧 Distribution but any distro can use it provided you installed awesome,alacritty and you have bash installed (and X11 but i am sure you have it)
[Scrn1png](https://gitlab.com/HackerHenry/dotfiles/-/blob/master/scrn1.png "scrnshot")
[Scrn2png](https://gitlab.com/HackerHenry/dotfiles/-/blob/master/scrn2.png "scrnshot")
## to install necessary software
# Icon theme: Material Black Lime 
* Debian based: sudo apt install awesome alacritty
* Arch based:   sudo pacman -S awesome alacritty
* Other: Just search for the package and install it with your package manager
## to setup the configs
1. cp /path/to/dotfiles/alacritty ~/.config # copy alacritty conf to configs dir
1. cp /path/to/dotfiles/awesome   ~/.config # copy awesome conf to config dir
1. cp /path/to/dotfiles/bash/banner ~/.banner # copy the banner file to your home dir,and make it invisible
1. cp /path.../wallpaper.jpg /usr/share/themes # copy the wallpaper 
# to access the banner just press CTRL+H in any GUI file manager (nautilus,thunar,caja...) and it'll show up
# to use it,you have to log out,and find a gear-ish looking icon,click and select the AwesomeWm setting.
# I hope you have fun installing and using my configs.
# TODO : to make your desktop really shine when you start to make
# your own custom configs (don't worry,its easy) keep in mind
# to stay with a colorscheme. There are hundreds of sites where you
# upload a picture and it extracts the colors. use them in your
# configs,and have fun! And if you already have a nice colorscheme,
# for beginners I advise using i3. There is a really good website 
# in this address: https://thomashunter.name/i3-configurator/ .
# If you are new to Linux and have a slow machine I'd use dwm instead. Hard to configure,but runs on everithing from old laptops to a smart fridge.
# IMPORTANT!! If you have problems with the opacity, type 'killall compton' to the terminal & hit enter. BTW the compton keybinding is MOD+CTRL+C